//
//  AppSyncronizer.swift
//  MainFramework
//
//  Created by Ale on 12/4/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit

protocol AppSyncProtocol {
    
}

class AppSyncronizer {
    //Typealias generales
    typealias OnSuccess = (_ data: [String: Any]) -> Void
    typealias OnError = (_ error: Error) -> Void
    typealias OnComplete = () -> Void
    
    //Typealias para los modelos
    typealias OnSuccessSingle<T> = (_ data: T) -> Void
    typealias OnSuccessMultiple<T> = (_ data: [T]) -> Void
    
    //Default callback responses
    static let OnSuccessReturn = {(_ data: Any) -> Void in }
    static let OnErrorReturn = {(_ error: Error) -> Void in }
    static let OnCompleteReturn = {}
}
