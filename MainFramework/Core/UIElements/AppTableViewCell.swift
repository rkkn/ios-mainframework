//
//  AppTableViewCell.swift
//  MainFramework
//
//  Created by Reki Haaku on 10/12/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit

class AppTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        // update UI
        accessoryType = selected ? .checkmark : .none
    }
}
