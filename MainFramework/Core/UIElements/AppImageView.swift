//
//  AppImageView.swift
//  MainFramework
//
//  Created by Reki Haaku on 10/12/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit

@IBDesignable class AppImageView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
            clipsToBounds = newValue > 0
        }
    }
}
