//
//  AppButton.swift
//  MainFramework
//
//  Created by Alexis Nilo on 10/9/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit

@IBDesignable class AppButton: UIButton
{
    override func layoutSubviews() {
        super.layoutSubviews()

        // Asign initial transparency
        setAlpha()

        // Asing alignment
        setImageAlignment()
    }

    override var isEnabled: Bool {
        didSet {
            setAlpha()
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }

    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }

    enum ImageAlign: String {
        case Left = "left"
        case Right = "right"
    }

    var imageAlignVar: ImageAlign?
    @available(*, unavailable, message: "This property is reserved for Interface Builder. Use 'Left', 'Right' or 'Default' instead.")
    @IBInspectable var imageAlign: String? {
        willSet {
            // Ensure user enters a valid shape while making it lowercase.
            // Ignore input if not valid.
            if let newImageAlign = ImageAlign(rawValue: newValue?.lowercased() ?? "") {
                imageAlignVar = newImageAlign
            }
        }
    }

    func setAlpha() {
        alpha = isEnabled ? 1.0 : 0.5
    }

    func setImageAlignment() {
        if imageView != nil {
            //print(bounds.width)
            //print(imageView?.frame.width)
            let borderRight: CGFloat = 16 //contentEdgeInsets.right
            //let borderLeft: CGFloat = 0 //contentEdgeInsets.left

            if imageAlignVar == ImageAlign.Right {
                imageEdgeInsets = UIEdgeInsets(top: 0, left: (bounds.width - (imageView?.frame.width)! - borderRight), bottom: 0, right: 0)
                titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (imageView?.frame.width)! + borderRight)
            }
            if imageAlignVar == ImageAlign.Left {
                //imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: (bounds.width - (imageView?.frame.width)!))
                //titleEdgeInsets = UIEdgeInsets(top: 0, left: (imageView?.frame.width)!, bottom: 0, right: 0)
            }
        }
    }
}
