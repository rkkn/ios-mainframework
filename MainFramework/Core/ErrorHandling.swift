//
//  ErrorHandling.swift
//  MainFramework
//
//  Created by Alexis Nilo on 12-09-18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import Foundation

// Protocolo para errores customizados
protocol OurErrorProtocol: LocalizedError {
    var title: String? { get }
    var code: Int { get }
}

// Errores customizados para la aplicacion
struct CustomError: OurErrorProtocol {
    var title: String?
    var code: Int
    var errorDescription: String? { return _description }
    var failureReason: String? { return _description }
    
    private var _description: String
    
    init(title: String?, description: String, code: Int) {
        self.title = title ?? "Error"
        self._description = description
        self.code = code
    }
}
