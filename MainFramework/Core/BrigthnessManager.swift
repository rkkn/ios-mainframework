//
//  BrigthnessManager.swift
//  MainFramework
//
//  Created by Alexis Nilo on 12-09-18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import Foundation
import UIKit

class BrightnessManager {
    // MARK: - Properties
    static let shared = BrightnessManager()
    
    // Se guarda el brillo al inicializar esta variable
    var brightValue: CGFloat = UIScreen.main.brightness
    
    enum BrighLevel: CGFloat {
        case Normal = 0.5
        case High = 1.0
        case Low = 0.2
    }
    var brightLevel = BrighLevel.Normal
    
    // Initialization
    private init() {
        
    }
    
    func setHigh(){
        if brightLevel == BrighLevel.Normal {
            brightValue = UIScreen.main.brightness
        }
        brightValue = BrighLevel.High.rawValue
        brightLevel = BrighLevel.High
    }
    
    func setLow(){
        if brightLevel == BrighLevel.Normal {
            brightValue = UIScreen.main.brightness
        }
        brightValue = BrighLevel.Low.rawValue
        brightLevel = BrighLevel.Low
    }
    
    func setNormal(){
        UIScreen.main.brightness = brightValue
        brightLevel = BrighLevel.Normal
    }
}
