//
//  GPSPositionManager.swift
//  MainFramework
//
//  Created by Alexis Nilo on 12-09-18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class GPSPositionManager: NSObject {
    // MARK: - Properties
    static let shared = GPSPositionManager()
    
    //geolocalizacion
    private let geo = CLLocationManager()
    var latitude: CLLocationDegrees = 0
    var longitude: CLLocationDegrees = 0
    
    var isEnable: Bool {
        get {
            if CLLocationManager.locationServicesEnabled() {
                switch CLLocationManager.authorizationStatus() {
                case .notDetermined, .restricted, .denied:
                    return false
                default:
                    return true
                }
            } else {
                return false
            }
        }
    }
    
    private var active: Bool = false
    var isActive: Bool {
        get {
            if (isEnable){
                return active
            }
            return false
        }
        set {
            if(!isEnable){
                active = false
                geo.stopUpdatingLocation()
                return
            }
            
            if(newValue){
                //Se marca como desactivado para que se active cuando lea correctamente la ubicacion
                active = false
                //Obtener GeoPosicion
                geo.startUpdatingLocation()
            } else {
                geo.stopUpdatingLocation()
                //Se marca como desactivado despues de detener el servicio
                active = false
            }
        }
    }
    
    // Initialization
    override private init() {
        super.init()
        
        geo.delegate = self
        geo.requestWhenInUseAuthorization()
        geo.desiredAccuracy = kCLLocationAccuracyBest
        geo.distanceFilter = kCLDistanceFilterNone
    }
    
    func getLat() -> CLLocationDegrees {
        return active ? latitude : 0
    }
    
    func getLng() -> CLLocationDegrees{
        return active ? longitude : 0
    }
}

extension GPSPositionManager: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location_ = locations.last else {
            self.latitude = 0
            self.longitude = 0
            active = false
            return
        }
        self.latitude = location_.coordinate.latitude
        self.longitude = location_.coordinate.longitude
        active = true
    }
}
