//
//  ActiveTimeTimerManager.swift
//  MainFramework
//
//  Created by Alexis Nilo on 05-10-18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import Foundation

//Esta clase indica cuanto tiempo lleva la app en pantalla o en background
class ActiveTimeTimerManager {
    // MARK: - Properties
    static let shared = ActiveTimeTimerManager()
    
    enum State {
        case background
        case foreground
    }
    
    var state = State.foreground
    
    //Timer para marcar cuando se va a background y cuando se sale de background
    private var markerDate = Date()
    
    //Contador de tiempo total
    private var totalRunningTime: Double = 0
    private var totalBackgroundTime: Double = 0
    
    //Contador del tiempo actual
    private var currentRunningTime: Double = 0
    private var currentBackgroundTime: Double = 0
    
    private init(){
        
    }
    
    func start(){
        state = .foreground
        markerDate = Date()
    }
    
    func stop(){
        // Se detiene la app. Se hace un goToBackground o goToForeground para guardar el tiempo
        //      de la sesion actual en los totales
        if ActiveTimeTimerManager.shared.state == .foreground {
            ActiveTimeTimerManager.shared.goToBackground()
        } else {
            ActiveTimeTimerManager.shared.goToForeground()
        }
    }
    
    func goToForeground(){
        state = .foreground
        currentBackgroundTime = Date().timeIntervalSince(markerDate)
        totalBackgroundTime += currentBackgroundTime
        markerDate = Date()
    }
    
    func goToBackground(){
        state = .background
        currentRunningTime = Date().timeIntervalSince(markerDate)
        totalRunningTime += currentRunningTime
        markerDate = Date()
    }
    
    func getCurrentForegroundTime() -> Double {
        // Si esta actualmente en foreground, mostrar el tiempo actual
        if ActiveTimeTimerManager.shared.state == .foreground {
            return Date().timeIntervalSince(markerDate)
        }
        
        //Si esta en background, mostar el tiempo guardado del estado foreground anterior
        return currentRunningTime
    }
    
    func getCurrentBackgroundTime() -> Double {
        // Si esta actualmente en background, mostrar el tiempo actual
        if ActiveTimeTimerManager.shared.state == .background {
            return Date().timeIntervalSince(markerDate)
        }
        
        //Si esta en foreground, mostar el tiempo guardado del estado background anterior
        return currentBackgroundTime
    }
    
    func getTotalForegroundTime() -> Double {
        return totalRunningTime
    }
    
    func getTotalBackgroundTime() -> Double {
        return totalBackgroundTime
    }
}
