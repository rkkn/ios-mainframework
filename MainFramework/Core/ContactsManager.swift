
//
//  ContactsManager.swift
//  MainFramework
//
//  Created by Alexis Nilo on 10/19/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit
import Contacts

class ContactsManager: NSObject {
    //Response definitions
    typealias OnSuccess = (_ data: [Contact]) -> Void
    typealias OnError = (_ error: Error) -> Void
    
    //Default callback responses
    static let OnSuccessReturn = {(_ data: [Contact]) -> Void in }
    static let OnErrorReturn = {(_ error: Error) -> Void in }
    
    //Estructura para almacenar los numeros
    struct Contact {
        var name: String
        var mobile: String
        var image: UIImage
        
        init(name: String, mobile: String, image: UIImage? = nil) {
            self.name = name
            self.mobile = mobile
            
            self.image = UIImage(named: "Settings.avatar")!
            if image != nil {
                self.image = image!
            }
        }
        
        init(givenName: String, familyName: String, phoneNumber: String, thumbnailImageData: Data?){
            self.name = "\(givenName) \(familyName)"
            self.mobile = phoneNumber
            self.image = UIImage(named: "Settings.avatar")!
            if thumbnailImageData != nil {
                self.image = UIImage(data: thumbnailImageData!) ?? UIImage(named: "Settings.avatar")!
            }
        }
    }
    
    static func getContacts(onSuccess: @escaping OnSuccess, onError: OnError? = OnErrorReturn){
        let status = CNContactStore.authorizationStatus(for: .contacts)
        if status == .denied || status == .restricted {
            return DispatchQueue.main.async {
                onError!(CustomError(title: "", description: "No se ha podido acceder a la agenda", code: 0))
            }
        }
        
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { granted, error in
            guard granted else {
                return DispatchQueue.main.async {
                    onError!(CustomError(title: "", description: "No se ha podido acceder a la agenda", code: 0))
                }
            }
            
            var contacts = [CNContact]()
            do {
                let contactsFetchRequest = CNContactFetchRequest(keysToFetch: [
                    CNContactGivenNameKey as CNKeyDescriptor,
                    CNContactFamilyNameKey as CNKeyDescriptor,
                    CNContactThumbnailImageDataKey as CNKeyDescriptor,
                    CNContactPhoneNumbersKey as CNKeyDescriptor
                    ])
                try store.enumerateContacts(with: contactsFetchRequest, usingBlock: { (cnContact, error) in
                    contacts.append(cnContact)
                })
            } catch {}
            
            var returnContacts = [Contact]()
            for contact in contacts {
                for number in contact.phoneNumbers {
                    returnContacts.append(Contact(givenName: contact.givenName, familyName: contact.familyName, phoneNumber: number.value.stringValue, thumbnailImageData: contact.thumbnailImageData))
                }
            }
            
            DispatchQueue.main.async {
                onSuccess(returnContacts)
            }
        }
    }
}
