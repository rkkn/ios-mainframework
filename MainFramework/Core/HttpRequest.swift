//
//  HttpRequest.swift
//  MainFramework
//
//  Created by Alexis Nilo on 02-03-18.
//  Copyright © 2018 safecard. All rights reserved.
//

import Foundation
import UIKit

class HttpRequest {
    //Singleton
    static let shared = HttpRequest()
    
    //Headers
    static private var headers: [String:String] = [:]
    
    //Constructor
    init() {}
    
    //Response definitions
    typealias OnSuccess = (_ data: [String: Any]) -> Void
    typealias OnError = (_ error: Error) -> Void
    typealias OnComplete = () -> Void
    
    //Default callback responses
    static let OnSuccessReturn = {(_ data: Any) -> Void in }
    static let OnErrorReturn = {(_ error: Error) -> Void in }
    static let OnCompleteReturn = {}
    
    public func getUrlRequest(url: String, params: [String: String]? = nil, body: [String: Any]? = nil) throws -> NSMutableURLRequest {
        var components = URLComponents(string: url)!
        components.queryItems = []
        if params != nil {
            for (key, value) in params! {
                components.queryItems?.append(URLQueryItem(name: key, value: value))
            }
        }
        components.queryItems?.append(URLQueryItem(name: "time", value: "\(Date().timeIntervalSince1970)"))
        components.percentEncodedQuery = components.percentEncodedQuery?.replacingOccurrences(of: "+", with: "%2B")
        let urlRequest = NSMutableURLRequest(url: components.url!)
        
        //Incluir headers
        for (key, value) in HttpRequest.headers {
            urlRequest.setValue(value, forHTTPHeaderField: key)
        }
        
        //Timeout for request
        urlRequest.timeoutInterval = 20
        
        if let body = body {
            urlRequest.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
            do {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: body, options: [])
            } catch {
                throw CustomError(title: "EmptyBodyError", description: "Cannot create JSON from body", code: 0)
            }
        }
        
        return urlRequest
    }
    
    //  MARK: - Generic HTTP Request for use in all HTTPs verbs
    private func http(urlRequest: NSMutableURLRequest, onSuccess: OnSuccess?, onError: OnError?, onComplete: OnComplete?){
        print("Request ==================================================")
        print(urlRequest.url!)
        print("==========================================================")
        
        let session = URLSession(configuration: URLSessionConfiguration.default)
        
        //Run NetworkActivityIndicator
        DispatchQueue.main.async(execute: {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        })
        
        session.dataTask(with: urlRequest as URLRequest){ (data, response, error) in
            //Stop NetworkActivityIndicator
            DispatchQueue.main.async(execute: {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            })
            
            // check for any errors
            guard error == nil else {
                print("error calling GET on \(urlRequest.url!)")
                DispatchQueue.main.async { onError!(CustomError(title: "URLRequest", description: (error?.localizedDescription)!, code: 0)) }
                return DispatchQueue.main.async { onComplete!() }
            }
            
            // make sure we got data
            guard let responseData = data else {
                DispatchQueue.main.async { onError!(CustomError(title: "EmptyDataError", description: "Did not receive data", code: 0)) }
                return DispatchQueue.main.async { onComplete!()}
            }
            
            // parse the result as JSON, since that's what the API provides
            do {
                guard let jsonData = try JSONSerialization.jsonObject(with: responseData, options: [])
                    as? [String: Any] else {
                        onError!(CustomError(title: "JsonDataError", description: "JSON serialization error", code: 0))
                        return DispatchQueue.main.async { onComplete!() }
                }
                
                DispatchQueue.main.async { onSuccess!(jsonData) }
            } catch  {
                DispatchQueue.main.async { onError!(CustomError(title: "JsonConvertionError", description: "Error trying to convert data to JSON", code: 0)) }
            }
            
            return DispatchQueue.main.async { onComplete!() }
        }.resume()
    }
    
    //  MARK: - GET Implementation
    func get(urlRequest: NSMutableURLRequest, onSuccess: OnSuccess?, onError: OnError? = OnErrorReturn, onComplete: OnComplete? = OnCompleteReturn) {
        urlRequest.httpMethod = "GET"
        self.http(urlRequest: urlRequest, onSuccess: onSuccess, onError: onError, onComplete: onComplete)
    }
    
    //  MARK: - POST Implementation
    func post(urlRequest: NSMutableURLRequest, onSuccess: OnSuccess?, onError: OnError? = OnErrorReturn, onComplete: OnComplete? = OnCompleteReturn){
        urlRequest.httpMethod = "POST"
        self.http(urlRequest: urlRequest, onSuccess: onSuccess, onError: onError, onComplete: onComplete)
    }
    
    //  MARK: - PUT Implementation
    func put(urlRequest: NSMutableURLRequest, onSuccess: OnSuccess?, onError: OnError? = OnErrorReturn, onComplete: OnComplete? = OnCompleteReturn){
        urlRequest.httpMethod = "PUT"
        self.http(urlRequest: urlRequest, onSuccess: onSuccess, onError: onError, onComplete: onComplete)
    }
    
    //  MARK: - DELETE Implementation
    func delete(urlRequest: NSMutableURLRequest, onSuccess: OnSuccess?, onError: OnError? = OnErrorReturn, onComplete: OnComplete? = OnCompleteReturn){
        urlRequest.httpMethod = "DELETE"
        self.http(urlRequest: urlRequest, onSuccess: onSuccess, onError: onError, onComplete: onComplete)
    }
}

