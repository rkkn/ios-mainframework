//
//  AppTabBarController.swift
//  MainFramework
//
//  Created by Reki Haaku on 10/12/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit

class AppTabBarController: UITabBarController {

}

// Modify preferredSatusBarStyle on all UIViewControllers
extension AppTabBarController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
