//
//  AppMultiSelectionViewController.swift
//  MainFramework
//
//  Created by Alexis Nilo on 10/18/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit

// Protocolo para poder seleccionar multiples elementos de una lista
protocol MultiSelectTableViewDelegated: AnyObject {
    func selectField(_ field: String, value: [Int])
}

class AppMultiSelectionViewController: AppTableViewController {
    weak var delegate: MultiSelectTableViewDelegated?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
