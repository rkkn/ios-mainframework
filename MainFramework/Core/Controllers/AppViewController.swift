//
//  AppViewController.swift
//  MainFramework
//
//  Created by Alexis Nilo on 11-09-18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit
import Toast_Swift
import BRYXBanner

class AppViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Activity loading
    var overlay: UIAlertController?
    func showActivityIndicator(message: String = "Cargando..."){
        if overlay == nil {
            overlay = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
            loadingIndicator.hidesWhenStopped = true
            loadingIndicator.style = UIActivityIndicatorView.Style.gray
            loadingIndicator.startAnimating()
            
            overlay!.view.addSubview(loadingIndicator)
            self.present(overlay!, animated: true, completion: nil)
        }
    }
    
    typealias HideActivityCompletion = () -> Void
    static let HideActivityReturn = {() -> Void in }
    func hideActivityIndicator(completion: @escaping HideActivityCompletion = HideActivityReturn) {
        // Stop activity loader and remove loadingView
        if overlay != nil {
            overlay!.dismiss(animated: false, completion: completion)
            overlay = nil
        }
    }
    
    func showError(_ error: Error){
        if overlay == nil { //Si no hay un mensaje, mostrar la notificacion diretamente
            showErrorHandler(error)
        } else {
            hideActivityIndicator {
                self.showErrorHandler(error)
            }
        }
    }
    
    //modificar como se muestra el error aca
    func showErrorHandler(_ error: Error){
        if let error = error as? CustomError {
            return iosNotification(title: error.title!, message: error.localizedDescription)
        }
        
        iosNotification(title: "", message: error.localizedDescription)
    }
    
    //Preparar para mostrar la siguiente vista
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        // Si es una lista seleccionable,
        if let nextViewController = segue.destination as? AppSingleSelectionViewController {
            nextViewController.delegate = self as? SingleSelectTableViewDelegated
        }
        if let nextViewController = segue.destination as? AppMultiSelectionViewController {
            nextViewController.delegate = self as? MultiSelectTableViewDelegated
        }
    }
    
    //Para hacer dismiss o popViewController segun corresponda
    func safeDismiss(animated: Bool){
        if let nvc = navigationController {
            nvc.popViewController(animated: animated)
        } else { // otherwise, dismiss it
            dismiss(animated: animated, completion: nil)
        }
    }
}

// Notifications on all ViewControllers
extension UIViewController {
    func iosNotification(title: String, message: String, delay: Double = 0) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        if (delay > 0){
            let time = DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: time, execute: {
                alert.dismiss(animated: true, completion: nil)
            })
        }
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func toastNotification(message: String, delay: Double = 10) {
        self.view.makeToast(message, duration: delay, position: .bottom)
    }
    
    typealias didTapBlock = () -> Void
    static let didTapBlockReturn = {}
    func bannerNotification(title: String, message: String, duration: Double, didTapBlock: didTapBlock? = didTapBlockReturn){
        let banner = Banner(title: title, subtitle: message, image: UIImage(), backgroundColor: UIColor.init(red: 25, green: 25, blue: 25, alpha: 1))
        
        banner.titleLabel.font = UIFont(name: "Roboto-Black", size: 15)
        banner.detailLabel.font =  UIFont(name: "Roboto-Light", size: 15)
        banner.dismissesOnTap = true
        banner.didTapBlock = didTapBlock
        banner.show(duration: duration)
    }
}

// Modify preferredSatusBarStyle on all UIViewControllers
extension AppViewController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

// ChangeNextView
extension UIViewController {
    func appNextView(viewController: UIViewController? = nil, animated: Bool = true){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.nextView(viewController: viewController, animated: animated)
    }
}

// Dismiss keyboard
extension UIViewController {
    func hideKeyboard(){
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard(){
        view.endEditing(true)
    }
}
