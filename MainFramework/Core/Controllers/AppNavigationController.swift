//
//  AppNavigationController.swift
//  MainFramework
//
//  Created by Reki Haaku on 10/12/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit

class AppNavigationController: UINavigationController {

}

// Modify preferredSatusBarStyle on all UIViewControllers
extension AppNavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
