//
//  AppSingleSelectionViewController.swift
//  MainFramework
//
//  Created by Alexis Nilo on 10/18/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit

// Protocolo para poder seleccionar un elemento de una lista
protocol SingleSelectTableViewDelegated: AnyObject {
    func selectField(_ field: String, value: Int)
}

class AppSingleSelectionViewController: AppTableViewController {
    weak var delegate: SingleSelectTableViewDelegated?
    
    //Elemento seleccionado por defecto
    var selectedId: Int? = 0
}

extension AppSingleSelectionViewController {
    // Si se presiona un elemento no seleccionado
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        safeDismiss(animated: true)
    }
    
    // Si se presiona el elemento seleccionado, se ejecuta un select sobre si mismo
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.tableView(tableView, didSelectRowAt: indexPath)
    }
}
