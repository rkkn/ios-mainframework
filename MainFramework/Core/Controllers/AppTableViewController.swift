//
//  AppTableViewController.swift
//  MainFramework
//
//  Created by Alexis Nilo on 11-09-18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit

class AppTableViewController: AppViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension AppTableViewController: UITableViewDelegate {
    
}

extension AppTableViewController: UIScrollViewDelegate {
    
}
