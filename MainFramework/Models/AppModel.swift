//
//  AppModel.swift
//  MainFramework
//
//  Created by Alexis Nilo on 10/8/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import Foundation
import SQLite

//Protocolo para AppModel.AppModel
protocol AppModelProtocol {
    associatedtype modelDataObject
    
    //Funciones genericas que se utilizaran para otras funciones
    static func setRow(row: Row) -> modelDataObject
    static func getBy(query: Table) throws -> [modelDataObject]
    
    //CRUD
    static func get(id: Int) throws -> modelDataObject
    static func save(object: modelDataObject) throws -> Int
    static func delete(id: Int) throws -> Bool
}

protocol AppModelListProtocol {
    associatedtype modelListDataObject
    
    //Funciones necesarias para listados
    static func getScalar(scalar: ScalarQuery<Int>) -> Int
    
    //Para contar cuantos elementos hay
    static func count() -> Int
    
    //Funcion que devuelve 1 objeto segun su offset en getAll.
    static func get(offset: Int) throws -> modelListDataObject?
    
    //Funcion para devolver todos los elementos
    static func getAll() throws -> [modelListDataObject]
}

//Clase para que los modelos puedan heredar de AppModel

class AppModel {
    // This is a private class
    internal init() {}
    
    //Inicializacion de la base de datos
    static let db = Database()?.db
    
    //Funciones que afectan a 1 elemento
    internal static func appModelSave(query: Table, or: OnConflict?, fields: [Setter]) throws -> Int {
        do {
            let id = try db?.run(query.insert(or: or ?? .replace, fields))
            if id != nil {
                return Int(id!)
            }
        } catch {
            print(error)
        }
        throw CustomError(title: "", description: "No se ha guardado el elemento", code: 0)
    }
    
    internal static func appModelGet(query: Table) throws -> Row {
        do {
            if let row = try db?.pluck(query) {
                return row
            }
        } catch {
            print(error)
        }
        throw CustomError(title: "", description: "No se ha encontrado el elemento", code: 0)
    }
    
    internal static func appModelGetBy(query: Table) throws -> [Row] {
        var returnArray: [Row] = [] //Devolver resultados
        do {
            for row in try (db?.prepare(query))! {
                returnArray.append(row)
            }
        } catch {
            print(error)
            throw CustomError(title: "", description: "Ha ocurrido un error al procesar la información", code: 0)
        }
        return returnArray
    }
    
    internal static func appModelGetScalar(query: Table, expression: Expression<Int?>) -> Int {
        do {
            if let value = try db?.scalar(query.select(expression)){
                return value
            }
        } catch {}
        return 0
    }
    
    internal static func appModelGetScalar(scalar: ScalarQuery<Int>) -> Int {
        do {
            if let value = try db?.scalar(scalar){
                return value
            }
        } catch {}
        return 0
    }
    
    internal static func appModelDelete(query: Table) throws {
        try db?.run(query.delete())
    }
}
