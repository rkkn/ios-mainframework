//
//  UserModel.swift
//  MainFramework
//
//  Created by Reki Haaku on 10/24/18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit
import SQLite

class UserModel: AppModel {
    //Singleton
    let shared = UserModel()
    private override init(){
        super.init()
    }
    
    class Model: AppModel.AppModelValue {
        var name: String?
        
        override init() {
            super.init()
        }
        
        override init(id: Int64) {
            super.init(id: id)
        }
        
        init(id: Int64, name: String){
            super.init(id: id)
            self.name = name
        }
    }
}

extension UserModel.Model: AppModelProtocolDelegate {    
    typealias model = UserModel.Model
    
    func get() -> UserModel.Model? {
        if let row = super.get(id: self.id!) {
            self.name = row[Expression<String>("name")]
        }
        return self
    }
    
    func get(filters: [Expression<Bool>], order: [Expressible]? = nil) -> [UserModel.Model] {
        var response: [UserModel.Model] = []
        for row in super.get(filters: filters, order: order){
            response.append(UserModel.Model(
                id: row[Expression<Int64>("id")],
                name: row[Expression<String>("name")]
            ))
        }
        return response
    }
    
    
    func save(or: OnConflict = .replace) -> Int64? {
        return super.save(or: .replace, fields: [
            Expression<Int64?>("id") <- self.id,
            Expression<String>("name") <- self.name!
        ])
    }
    
    func save() -> Void {
        let _: Int64? = self.save()
    }
}
