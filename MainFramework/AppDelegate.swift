//
//  AppDelegate.swift
//  MainFramework
//
//  Created by Alexis Nilo on 11-09-18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit
import Instabug
import Mixpanel

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Clear local data if UITesting
        if ProcessInfo.processInfo.arguments.contains("UITests") {
            deleteLocalData()
        }
        
        // Override point for customization after application launch.
        ActiveTimeTimerManager.shared.start()
        
        /* Iniciar instabug */
        Instabug.start(withToken: "6aa7404d1d110bd4130a3272ea64070f", invocationEvents: [.none])
        BugReporting.promptOptions = [.bug, .feedback]
        
        /* Iniciar mixpanel */
        Mixpanel.initialize(token: "fd4450d5205590e2e894cf2ad3d01ec1")
        
        /* Track de abrir la app */
        Mixpanel.mainInstance().track(event: "openApp")
        
        //Inicializacion de la base de datos
        guard let database = Database() else {
            fatalError("could not setup database")
        }
        
        do {
            try database.migrateIfNeeded()
        } catch {
            fatalError("failed to migrate database: \(error)")
        }
        
        // Select Storyboard
        nextView()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        // Estadisticas de uso
        ActiveTimeTimerManager.shared.goToBackground()
        Mixpanel.mainInstance().track(event: "goToBackground", properties: ["activeTime": ActiveTimeTimerManager.shared.getCurrentForegroundTime()])
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
        // Estadisticas de uso
        ActiveTimeTimerManager.shared.goToForeground()
        Mixpanel.mainInstance().track(event: "goToForeground", properties: ["inactiveTime": ActiveTimeTimerManager.shared.getCurrentBackgroundTime()])
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        
        // Estadisticas de uso
        ActiveTimeTimerManager.shared.stop()
        Mixpanel.mainInstance().track(event: "appTerminate", properties: ["totalActiveTime": ActiveTimeTimerManager.shared.getTotalForegroundTime(), "totalInactiveTime": ActiveTimeTimerManager.shared.getTotalBackgroundTime()])
    }
    
    func nextView(viewController: UIViewController? = nil, animated: Bool? = true){
        //Asign default view
        var nextViewController = viewController
        
        if nextViewController == nil {
            //Decide what view will shows
            self.window?.rootViewController = ViewController.instantiate(fromAppStoryboard: .Main)
            if UserDefaults.standard.bool(forKey: "GoToSecondStoryboard") {
                self.window?.rootViewController = SecondViewController.instantiate(fromAppStoryboard: .Second)
            }
        }
        
        if (viewController != nil && animated == true){
            UIView.transition(
                from: (self.window?.rootViewController!.view!)!,
                to: (nextViewController!.view!),
                duration: 0.25,
                options: .transitionCrossDissolve,
                completion: {
                    finished in self.window?.rootViewController = nextViewController
            }
            )
        } else {
            self.window?.rootViewController = nextViewController
            self.window?.makeKeyAndVisible()
        }
    }
    
    func deleteLocalData() {
        //Delete UserDefaults
        UserDefaults.standard.removePersistentDomain(forName: Bundle.main.bundleIdentifier!)
        //Delete Database file
        guard let documentsURL = URL(string: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]) else {
            fatalError("could not get user documents directory URL")
        }
        
        do {
            try FileManager.default.removeItem(at: documentsURL.appendingPathComponent("store.sqlite"))
        } catch {}
    }
}

