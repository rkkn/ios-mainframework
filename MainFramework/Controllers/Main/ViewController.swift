//
//  ViewController.swift
//  MainFramework
//
//  Created by Alexis Nilo on 11-09-18.
//  Copyright © 2018 Alexis Nilo. All rights reserved.
//

import UIKit

class ViewController: AppViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("Loaded")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.toastNotification(message: "Prueba de texto super mega hiper grande que ocupe todo el espacio de la pantalla y use doble linea para ser mostrado. Tiene que ocupar harto espacio")
        }
    }
    @IBAction func goToSecondStoryboard(_ sender: Any) {
        UserDefaults.standard.set(true, forKey: "GoToSecondStoryboard")
        self.present(SecondViewController.instantiate(fromAppStoryboard: .Second), animated: true)
    }
}

