# README #

### Configurar el repo ###
Se debe crear la carpeta, luego hacer un git init y agregar los remote
git remote add origin git@bitbucket.org:proyect/acount.git
touch README.md
git add README.md
git push origin master
git remote add upstream git@bitbucket.org:rkkn/mainframework.git
git fetch upstream
git checkout master
git merge upstream/master


### Modify app name ###
Step 1 - Rename the project
Click on the project you want to rename in the "Project navigator" on the left of the Xcode view.
On the right select the "File inspector" and the name of your project should be in there under "Identity and Type", change it to the new name.
Click "Rename" in a dropdown menu

Step 2 - Rename the Scheme
In the top bar (near "Stop" button), there is a scheme for your OLD product, click on it, then go to "Manage schemes"
Click on the OLD name in the scheme, and it will become editable, change the name

Step 3 - Rename the folder with your assets
Renombrar carpetas en Proyect navigator

Step 4 - Rename the Build plist data**
Click on the project in the "Project navigator" on the left, in the main panel select "Build Settings"
Search for "plist" in this section
Under packaging, you will see Info.plist, and Product bundle identifier
Rename the top entry in Info.plist
Do the same for Product Identifier

Step 4 - Renombrar Build Settings
Find in Proyect -> Each proyect and targets -> Build Settings -> Find by "MainFramework" and replace with proper name  


Step 5 - Renombrar Podfile
En el archivo Podfile, renombrar todos los "MainFramework" por el nuevo nombre del proyecto
luego se debe hacer un
pod deintegrate  
pod install  


### Fuentes ###
https://stackoverflow.com/questions/33370175/how-do-i-completely-rename-an-xcode-project-i-e-inclusive-of-folders  
https://stackoverflow.com/questions/15152632/ios-changing-target-name/16565796  
https://stackoverflow.com/questions/15152632/ios-changing-target-name  
https://stackoverflow.com/questions/39492974/tests-stop-working-under-xcode-8-test-host-error/39655865  
